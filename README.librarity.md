# stama-rx
[![version](https://img.shields.io/npm/v/stama-rx.svg?style=flat)](https://www.npmjs.com/package/stama-rx) [![npm](https://img.shields.io/npm/l/stama-rx.svg)](https://opensource.org/licenses/MIT)

## Installation

```bash
npm i -S stama-rx
```

## Example Structure

main.ts

{{./example/src/main.ts}}

stama/actions.ts

{{./example/src/stama/actions.ts}}

stama/effects.ts

{{./example/src/stama/effects.ts}}

To see the visualized flow of example, fork [this repository](https://bitbucket.org/dkhang97/stama-rx) then run the following commands

```bash
npm i
npm run example
```

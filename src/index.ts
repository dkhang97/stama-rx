import { BehaviorSubject, Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { filter, map, scan, shareReplay, takeUntil, withLatestFrom } from 'rxjs/operators';

export class Action<A extends string = string, P = unknown, S = any> {
    constructor(
        readonly type: A,
        readonly payload: P,
        readonly reducer: (state: S, payload: P) => S,
    ) { }
}

export interface ActionBuilder<A extends string, T> {
    type: A;
    create(payload?: T): Action<A, T>;
    dispatch(payload?: T): void;
}

export type ActionType<T> = T extends ActionBuilder<any, any> ? ReturnType<T['create']>
    : T extends { prototype: ActionBuilder<any, any> } ? ReturnType<T['prototype']['create']>
    : T extends Action ? T : never;

export interface CreateEffectSelectorCreator<S = any> {
    actions$: Observable<Action>,
    state$: Observable<S>,
    fromAction<FA>(...builders: (ActionBuilder<string, FA> | Action<string, FA>)[]): Observable<FA>,
}

export default class StamaRx<S> {
    private destroyed = false;
    private readonly subscriptions: Subscription[] = [];
    private readonly registeredEffects = new WeakSet<Observable<Action>>();
    private readonly completedEffects: Promise<Action>[] = [];

    private readonly completeSignalSbj = new BehaviorSubject(false);
    // readonly completeSignal = this.completeSignalSbj.pipe(first(c => !!c));
    private readonly completeActionTypeSbj = new Subject<string>();
    private readonly completeActionTypes = this.completeActionTypeSbj.pipe(
        scan((acc, type) => [...acc, type].filter((curr, idx, ary) => ary.indexOf(curr) === idx), [] as string[]),
        shareReplay(1),
    );

    private readonly actionsSbj = new ReplaySubject<Action>()
    private readonly stateSbj: BehaviorSubject<S>;
    readonly state: Observable<S>;
    readonly actions$ = this.actionsSbj.pipe(filter(a => typeof a?.type === 'string'));


    constructor(initState: Partial<S> = {}) {
        this.stateSbj = new BehaviorSubject(initState as any);
        this.state = this.actionsSbj.pipe(
            withLatestFrom(this.stateSbj),
            map(([action, state]) => {
                if (typeof action?.reducer === 'function') {
                    return action.reducer(state, action.payload);
                }

                return state;
            }),
            shareReplay(1),
        );
    }

    onAction<A extends string, TP>(...builders: (ActionBuilder<A, TP> | Action<A, TP>)[]): Observable<Action<A, TP>> {
        return this.actions$.pipe(
            filter(a => builders.some(b => b?.type === a.type)),
        ) as any;
    }

    actionBuilder<T = unknown>(reducer?: (state: S, payload: T) => S) {
        return {
            create: <A extends string>(type: A): ActionBuilder<A, T> => {
                const create = (payload?: T) => new Action<A, T>(type, payload, reducer);

                return {
                    type, create,
                    dispatch: (payload: T) => this.dispatch(create(payload)),
                };
            },
        }
    }

    dispatch(action: Action): void {
        if (typeof action?.reducer === 'function') {
            this.stateSbj.next(action.reducer(this.stateSbj.getValue(), action.payload));
        }
        this.actionsSbj.next(action);
    }

    createEffect<A extends Action>(selector: (creator: CreateEffectSelectorCreator<S>) => Observable<A>, opts?: {
        dispatch?: true,
        signalCompleteOnFinished?: ActionBuilder<string, any> | ActionBuilder<string, any>[],
    }): Observable<A>;
    createEffect(selector: (creator: CreateEffectSelectorCreator<S>) => Observable<unknown>, opts: {
        dispatch?: false,
        signalCompleteOnFinished?: ActionBuilder<string, any> | ActionBuilder<string, any>[],
    }): Observable<unknown>;
    createEffect<A extends Action>(selector: (creator: CreateEffectSelectorCreator) => Observable<A>, opts?: {
        dispatch?: boolean,
        signalCompleteOnFinished?: ActionBuilder<string, any> | ActionBuilder<string, any>[],
    }) {
        const obs = selector({
            actions$: this.actions$, state$: this.state,
            fromAction: (...builders) => this.onAction(...builders).pipe(
                map(a => a.payload),
                takeUntil(this.completeActionTypes.pipe(filter(types => builders.some(b => ~types.indexOf(b?.type))))),
            ) as any,
        });

        if (!this.destroyed && !this.registeredEffects.has(obs)) {
            const signalFinish = () => {
                if (opts?.signalCompleteOnFinished) {
                    [].concat(opts.signalCompleteOnFinished).forEach(a => this.completeActionTypeSbj.next(a.type));
                };
            }

            this.completedEffects.push(new Promise((resolve, reject) => {
                let resolved: boolean;
                const doResolve: typeof resolve = val => {
                    if (!resolved) {
                        resolved = true;
                        resolve(val);
                    }
                }


                this.subscriptions.push(obs.pipe(
                    withLatestFrom(this.completeSignalSbj),
                ).subscribe(([action, completeSignal]) => {
                    if (opts?.dispatch !== false) {
                        this.dispatch(action);
                    }

                    if (completeSignal) {
                        doResolve(action);
                    }
                }, err => {
                    resolved = true;
                    signalFinish();
                    reject(err);
                }, () => {
                    signalFinish();
                    doResolve(undefined);
                }));
            }));
            this.registeredEffects.add(obs);
        }

        return obs;
    }

    complete(action?: ActionBuilder<string, any>) {
        if (action) {
            this.completeActionTypeSbj.next(action.type);
        } else {
            this.completeSignalSbj.next(true);
        }
    }

    destroy() {
        if (this.destroyed) {
            return;
        }

        this.destroyed = true;
        this.actionsSbj.complete();
        this.complete();
    }

    untilFinished(): Promise<never>
    untilFinished(cb: (err?: Error, result?: unknown) => void): void
    untilFinished(cb?: (err?: Error, result?: unknown) => void) {
        const task = Promise.all(this.completedEffects);

        if (typeof cb === 'function') {
            task.then(actions => cb(undefined, actions)).catch(err => cb(err, undefined));
            return;
        }

        return task;
    }
}

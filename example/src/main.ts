import './stama/effects';

import faker from 'faker';

import { DeployAction, stamaInstance } from './stama/actions';

(async () => {
    const actionCount = faker.datatype.number({ min: 3, max: 5 });

    for (let i = 0; i < actionCount; i++) {
        DeployAction.dispatch({ projectName: faker.company.catchPhrase() });
    }
    stamaInstance.complete(DeployAction);

    await stamaInstance.untilFinished();
    console.log('🏁 Finished');
})();

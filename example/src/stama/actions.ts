import StamaRx from 'stama-rx';

export const stamaInstance = new StamaRx();

export const DeployAction = stamaInstance.actionBuilder<{
    projectName: string,
}>().create('Deploy');

export const BuildProjectAction = stamaInstance.actionBuilder<{
    projectName: string,
}>().create('Build Project');

export const UploadAction = stamaInstance.actionBuilder<{
    projectName: string,
    targetHost: string,
}>().create('Upload Project');

export const FinishAction = stamaInstance.actionBuilder<{
    projectName: string,
}>().create('Finish');
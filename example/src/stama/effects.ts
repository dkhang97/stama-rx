import faker from 'faker';
import { timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { BuildProjectAction, DeployAction, FinishAction, stamaInstance, UploadAction } from './actions';

export const deploy$ = stamaInstance.createEffect(s => s.fromAction(DeployAction).pipe(
    map(({ projectName }) => {
        return BuildProjectAction.create({ projectName });
    }),
), { signalCompleteOnFinished: BuildProjectAction });


export const build$ = stamaInstance.createEffect(s => s.fromAction(BuildProjectAction).pipe(
    mergeMap(async ({ projectName }) => {
        console.log(`📦 Start building '${projectName}'`);
        await timer(faker.datatype.number({ min: 500, max: 3000 })).toPromise();
        console.log(`📦 ✅ Project '${projectName}' built`);

        return UploadAction.create({ projectName, targetHost: faker.internet.domainName() });
    }),
), { signalCompleteOnFinished: UploadAction });


export const upload$ = stamaInstance.createEffect(s => s.fromAction(UploadAction).pipe(
    mergeMap(async ({ projectName, targetHost }) => {
        console.log(`🏭 Uploading project '${projectName}' to '${targetHost}'`);
        await timer(faker.datatype.number({ min: 500, max: 3000 })).toPromise();
        console.log(`🏭 ✅ Project '${projectName}' uploaded`);

        return FinishAction.create({ projectName });
    }),
));

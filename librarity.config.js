module.exports = {
    confirmOverwrite: false,
    extensions: {
        '.ts': 'typescript',
    },
};